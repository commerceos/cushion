#Couchbase Testing
This project is for testing the Couchbase

## How to start
Starting point could be: 

* src/main/java/org/ebayopensource/couchbase/admin/AdminConsole.java

From eclipse: Run as Java Application

## Open Issues
* On event that the last node in CB cluster got shutdown or in case when cluster with single node is shutdown, the following Null Pointer Exception is observed


	2014-08-03 10:44:15.863 INFO com.couchbase.client.CouchbaseConnection:  Reconnecting due to exception on {QA sa=localhost/127.0.0.1:11210, #Rops=1, #Wops=0, #iq=0, topRop=Cmd: 0 Opaque: 520 Key: testData, topWop=null, toWrite=0, interested=1}
java.io.IOException: Disconnected unexpected, will reconnect.
	at net.spy.memcached.MemcachedConnection.handleReadsWhenChannelEndOfStream(MemcachedConnection.java:911)
	at net.spy.memcached.MemcachedConnection.handleReads(MemcachedConnection.java:823)
	at net.spy.memcached.MemcachedConnection.handleReadsAndWrites(MemcachedConnection.java:720)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:683)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:436)
	at com.couchbase.client.CouchbaseConnection.run(CouchbaseConnection.java:324)
2014-08-03 10:44:15.866 WARN net.spy.memcached.protocol.binary.BinaryMemcachedNodeImpl:  Discarding partially completed op: Cmd: -75 Opaque: 0
2014-08-03 10:44:20.883 INFO com.couchbase.client.vbucket.provider.BucketConfigurationProvider:  Could not fetch config from http seed nodes.
java.lang.NullPointerException
	at com.couchbase.client.vbucket.ConfigurationProviderHTTP.subscribe(ConfigurationProviderHTTP.java:335)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.monitorBucket(BucketConfigurationProvider.java:361)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrapHttp(BucketConfigurationProvider.java:346)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrap(BucketConfigurationProvider.java:122)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.signalOutdated(BucketConfigurationProvider.java:502)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.reloadConfig(BucketConfigurationProvider.java:529)
	at com.couchbase.client.CouchbaseConnection.queueReconnect(CouchbaseConnection.java:362)
	at net.spy.memcached.MemcachedConnection.lostConnection(MemcachedConnection.java:634)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:703)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:436)
	at com.couchbase.client.CouchbaseConnection.run(CouchbaseConnection.java:324)
2014-08-03 10:44:20.884 INFO com.couchbase.client.vbucket.provider.BucketConfigurationProvider:  Could not fetch config from http seed nodes.
java.lang.NullPointerException
	at com.couchbase.client.vbucket.ConfigurationProviderHTTP.subscribe(ConfigurationProviderHTTP.java:335)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.monitorBucket(BucketConfigurationProvider.java:361)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrapHttp(BucketConfigurationProvider.java:346)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrap(BucketConfigurationProvider.java:122)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.signalOutdated(BucketConfigurationProvider.java:497)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider$2.connectionLost(BucketConfigurationProvider.java:277)
	at net.spy.memcached.MemcachedConnection.lostConnection(MemcachedConnection.java:636)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:703)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:436)
	at com.couchbase.client.CouchbaseConnection.run(CouchbaseConnection.java:324)
2014-08-03 10:44:20.885 INFO com.couchbase.client.vbucket.provider.BucketConfigurationProvider:  Could not load config from existing connection, rerunning bootstrap.
com.couchbase.client.vbucket.ConfigurationException: Could not fetch a valid Bucket configuration.
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrap(BucketConfigurationProvider.java:123)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.signalOutdated(BucketConfigurationProvider.java:502)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.reloadConfig(BucketConfigurationProvider.java:529)
	at com.couchbase.client.CouchbaseConnection.queueReconnect(CouchbaseConnection.java:362)
	at net.spy.memcached.MemcachedConnection.lostConnection(MemcachedConnection.java:634)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:703)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:436)
	at com.couchbase.client.CouchbaseConnection.run(CouchbaseConnection.java:324)
2014-08-03 10:44:25.888 INFO com.couchbase.client.vbucket.provider.BucketConfigurationProvider:  Could not fetch config from http seed nodes.
java.lang.NullPointerException
	at com.couchbase.client.vbucket.ConfigurationProviderHTTP.subscribe(ConfigurationProviderHTTP.java:335)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.monitorBucket(BucketConfigurationProvider.java:361)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrapHttp(BucketConfigurationProvider.java:346)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrap(BucketConfigurationProvider.java:122)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.signalOutdated(BucketConfigurationProvider.java:512)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.reloadConfig(BucketConfigurationProvider.java:529)
	at com.couchbase.client.CouchbaseConnection.queueReconnect(CouchbaseConnection.java:362)
	at net.spy.memcached.MemcachedConnection.lostConnection(MemcachedConnection.java:634)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:703)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:436)
	at com.couchbase.client.CouchbaseConnection.run(CouchbaseConnection.java:324)
Exception in thread "main" net.spy.memcached.OperationTimeoutException: Timeout waiting for value: waited 50 ms. Node status: Connection Status { localhost/127.0.0.1:11210 active: true, authed: true, last read: 1,055 ms ago }
	at net.spy.memcached.MemcachedClient.get(MemcachedClient.java:1240)
	at net.spy.memcached.MemcachedClient.get(MemcachedClient.java:1257)
	at org.ebayopensource.couchbase.data.DataObject.load(DataObject.java:38)
	at org.ebayopensource.couchbase.admin.AdminConsole.readTestData(AdminConsole.java:119)
	at org.ebayopensource.couchbase.admin.AdminConsole.imitateActivity(AdminConsole.java:82)
	at org.ebayopensource.couchbase.admin.AdminConsole.main(AdminConsole.java:75)
Caused by: net.spy.memcached.internal.CheckedOperationTimeoutException: Timed out waiting for operation - failing node: localhost/127.0.0.1:11210
	at net.spy.memcached.internal.OperationFuture.get(OperationFuture.java:167)
	at net.spy.memcached.internal.GetFuture.get(GetFuture.java:69)
	at net.spy.memcached.MemcachedClient.get(MemcachedClient.java:1230)
	... 5 more
Exception in thread "Memcached IO over {CouchbaseConfigConnection to localhost/127.0.0.1:11210}" com.couchbase.client.vbucket.ConfigurationException: Could not fetch a valid Bucket configuration.
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrap(BucketConfigurationProvider.java:123)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.signalOutdated(BucketConfigurationProvider.java:497)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider$2.connectionLost(BucketConfigurationProvider.java:277)
	at net.spy.memcached.MemcachedConnection.lostConnection(MemcachedConnection.java:636)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:703)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:436)
	at com.couchbase.client.CouchbaseConnection.run(CouchbaseConnection.java:324)
Exception in thread "Memcached IO over {MemcachedConnection to localhost/127.0.0.1:11210}" com.couchbase.client.vbucket.ConfigurationException: Could not fetch a valid Bucket configuration.
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.bootstrap(BucketConfigurationProvider.java:123)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.signalOutdated(BucketConfigurationProvider.java:512)
	at com.couchbase.client.vbucket.provider.BucketConfigurationProvider.reloadConfig(BucketConfigurationProvider.java:529)
	at com.couchbase.client.CouchbaseConnection.queueReconnect(CouchbaseConnection.java:362)
	at net.spy.memcached.MemcachedConnection.lostConnection(MemcachedConnection.java:634)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:703)
	at net.spy.memcached.MemcachedConnection.handleIO(MemcachedConnection.java:436)
	at com.couchbase.client.CouchbaseConnection.run(CouchbaseConnection.java:324)
`