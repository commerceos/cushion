package org.ebayopensource.couchbase.admin;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import net.spy.memcached.internal.OperationFuture;

import org.ebayopensource.couchbase.connection.Configuration;
import org.ebayopensource.couchbase.connection.ConfigurationException;
import org.ebayopensource.couchbase.connection.ConnectionDriver;
import org.ebayopensource.couchbase.data.DataObject;

import com.google.gson.Gson;

public class AdminConsole implements AdminConsoleMBean {
	
	ConnectionDriver driver = ConnectionDriver.getInstance();
	Configuration configuration = new ConfigurationHolder();
	Gson gson = new Gson();

	public String getDatasourcesAsJson() {
		return gson.toJson(driver.getDatasources());
	}

	public String getDatsourceConfig(String datasourceName) {
		return driver.getDatasources().get(datasourceName).getConfig();
	}

	public Object doGet(String datasourceName, String key) {
		try {
			return driver.getClient(datasourceName).get(key);
		} catch (IOException e) {
			return "IOException " + e;
		} catch (ConfigurationException e) {
			return "ConfigurationException " + e;
		}
	}

	public String doSet(String datasourceName, String key, String value) {
		OperationFuture<Boolean> operationFuture = null;
		try {
			operationFuture = driver.getClient(datasourceName).set(key, value);
		} catch (IOException e) {
			return "IOException " + e;
		} catch (ConfigurationException e) {
			return "ConfigurationException " + e;
		}
		return operationFuture.getStatus().getMessage();
	}

	public String doDelete(String datasourceName, String key) {
		OperationFuture<Boolean> operationFuture;
		try {
			operationFuture = driver.getClient(datasourceName).delete(key);
		} catch (IOException e) {
			return "IOException " + e;
		} catch (ConfigurationException e) {
			return "ConfigurationException " + e;
		}
		return operationFuture.getStatus().getMessage();
	}
	
	public static void main(String[] args) throws Exception {
		AdminConsoleMBean console = new AdminConsole();
		console.addLocalConnectionConfig();
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = new ObjectName("org.ebayopensources.couchbase.jmx:type=AdminConsoleMBean");		
		mbs.registerMBean(console, name);
		imitateActivity(console);
	}
	
	private static void imitateActivity(AdminConsoleMBean console) {
		while(true) {
			try {
				try {
					console.readTestData();
				} catch (Exception e) {
					e.printStackTrace();
				} 
				Thread.sleep(1000);
			}
			catch(InterruptedException e) { }
		}
	}

	public void addConfig(String key, String config) {
		configuration.setConfiguration(key, config);
		driver.setConfigurationProvider(configuration);;
	}

	public void addLocalConnectionConfig() {
		addConfig("local", "hosts=http://localhost:8091/pools\nbucket=default\npwd=");
	}

	public void removeDatasource(String datasourceName) throws IOException, ConfigurationException {
		driver.removeDatasource(datasourceName);
	}

	public void addTestData() throws IOException, ConfigurationException {
		DataObject data = new DataObject("local", "testData");
		data.getNvPairs().setProperty("one", "1");
		data.getNvPairs().setProperty("two", "2");
		data.getNvPairs().setProperty("three", "3");
		data.save();
	}

	public String readTestData() throws IOException, ConfigurationException {
		DataObject data = new DataObject("local", "testData");
		data.load();
		return data.getNvPairs().toString();
	}

}
