package org.ebayopensource.couchbase.admin;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.ebayopensource.couchbase.connection.Configuration;

public class ConfigurationHolder implements Configuration {
	
	ConcurrentMap<String, String> configurations = new ConcurrentHashMap<String, String>();

	public String getConfiguration(String key) {
		return configurations.get(key);
	}

	public void setConfiguration(String key, String config) {
		configurations.put(key, config);
	}

	
}
