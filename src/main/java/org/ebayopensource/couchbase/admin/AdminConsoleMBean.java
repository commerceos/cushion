package org.ebayopensource.couchbase.admin;

import java.io.IOException;
import java.util.List;

import org.ebayopensource.couchbase.connection.ConfigurationException;

public interface AdminConsoleMBean {

	public String getDatasourcesAsJson ();
	public String getDatsourceConfig (String datasorceName);
	public Object doGet(String datasourcName, String key);
	public String doSet(String datasourceName, String key, String value);
	public String doDelete(String datasourcName, String key);
	public void addConfig(String key, String config);
	public void removeDatasource(String key) throws IOException, ConfigurationException;
	public void addLocalConnectionConfig();
	public void addTestData() throws IOException, ConfigurationException;
	public String readTestData() throws IOException, ConfigurationException;
}
