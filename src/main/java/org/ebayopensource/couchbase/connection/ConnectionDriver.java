package org.ebayopensource.couchbase.connection;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.CouchbaseConnectionFactory;
import com.couchbase.client.CouchbaseConnectionFactoryBuilder;

import net.spy.memcached.ConnectionObserver;
import net.spy.memcached.internal.GetFuture;
import net.spy.memcached.internal.OperationFuture;

public class ConnectionDriver {

	private static ConnectionDriver instance = new ConnectionDriver();
	private ConcurrentMap<String, Datasource> datasources = new ConcurrentHashMap<String, Datasource>();
	private Configuration configuration = null;

	private ConnectionDriver() {

	}

	public static ConnectionDriver getInstance() {
		return instance;
	}

	public CouchbaseClient getClient(String datasourceName) throws IOException,
			ConfigurationException {
		Datasource datasource = datasources.get(datasourceName);
		if (datasource == null) {
			datasource = new Datasource();
			datasource.name = datasourceName;
			datasources.put(datasourceName, datasource);
		}
		if (datasource.client == null) {
			init(datasource);
		}
		return datasource.client;
	}

	public void removeDatasource(String datasourceName) throws IOException,
			ConfigurationException {
		Datasource datasource = datasources.get(datasourceName);
		if (datasource != null) {
			if (datasource.client != null) {
				datasource.client.shutdown();
				datasource.client = null;
			}
			if (datasource.connectionFactory != null) {
				datasource.connectionFactory = null;
			}
			if (datasource.config != null) {
				datasource.config = null;
			}
		}
	}

	public void setConfigurationProvider(Configuration config) {
		configuration = config;
	}

	public ConcurrentMap<String, Datasource> getDatasources() {
		return datasources;
	}

	private void init(Datasource datasource) throws IOException,
			ConfigurationException {
		if (configuration == null) {
			throw new ConfigurationException("Configuration source not set!");
		}

		String config = configuration.getConfiguration(datasource.name);
		if (datasource.getConfig() == null) {
			datasource.config = config;
		}
		if (datasource.client != null && datasource.config.equals(config)) {
			return;
		}
		Properties p = new Properties();
		p.load(new StringReader(config));
		List<URI> uris = new LinkedList<URI>();
		String hosts = p.getProperty("hosts");
		for (String host : hosts.split(",")) {
			uris.add(URI.create(host));
		}
		CouchbaseConnectionFactoryBuilder cfb = new CouchbaseConnectionFactoryBuilder();
		cfb.setOpTimeout(50);
		cfb.setTimeoutExceptionThreshold(3);
		CouchbaseConnectionFactory cf = cfb.buildCouchbaseConnection(uris,
				p.getProperty("bucket"), p.getProperty("pwd"));
		datasource.client = new CouchbaseClient(cf);
		datasource.connectionFactory = cf;
		ConnectionObserver connectionObserver = new ConnectionObserverInstance(
				datasource.name);
		datasource.client.addObserver(connectionObserver);
	}
}
