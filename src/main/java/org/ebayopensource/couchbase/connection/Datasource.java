package org.ebayopensource.couchbase.connection;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.CouchbaseConnectionFactory;

public class Datasource {
	
	String name = null;
	String config = null;
	CouchbaseClient client = null;
	CouchbaseConnectionFactory connectionFactory = null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public CouchbaseClient getClient() {
		return client;
	}
	public void setClient(CouchbaseClient client) {
		this.client = client;
	}
	public CouchbaseConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}
	public void setConnectionFactory(CouchbaseConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}
	
}
