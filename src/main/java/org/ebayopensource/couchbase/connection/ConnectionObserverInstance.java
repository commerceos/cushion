package org.ebayopensource.couchbase.connection;

import java.net.SocketAddress;

import net.spy.memcached.ConnectionObserver;

public class ConnectionObserverInstance implements ConnectionObserver {
	
	private String datasourceName;

	public ConnectionObserverInstance (String datasourceName){
		this.datasourceName = datasourceName;
		
	}

	public void connectionEstablished(SocketAddress sa, int reconnectCount) {
		// TODO Auto-generated method stub
		System.out.println ("Connected! sa="+sa);
	}

	public void connectionLost(SocketAddress sa) {
		// TODO Auto-generated method stub
		System.out.println ("Connection Lost! sa="+sa);
	}

}
