package org.ebayopensource.couchbase.connection;

public interface Configuration {
	
	public String getConfiguration(String key);
	public void setConfiguration(String key, String config);


}
