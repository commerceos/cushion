package org.ebayopensource.couchbase.connection;

public class ConfigurationException extends Exception {

	public ConfigurationException(String msg) {
		super(msg);
	}
	
}
