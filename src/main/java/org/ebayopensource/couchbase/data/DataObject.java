package org.ebayopensource.couchbase.data;

import java.io.IOException;
import java.util.Properties;

import org.ebayopensource.couchbase.connection.ConfigurationException;
import org.ebayopensource.couchbase.connection.ConnectionDriver;

import com.google.gson.Gson;

import net.spy.memcached.internal.OperationFuture;

public class DataObject {
	ConnectionDriver cDriver = ConnectionDriver.getInstance();
	String datasourceName;
	Properties nvPairs = new Properties();
	private String key;
	Gson gson = new Gson();
	
	public DataObject (String datasourceName, String key){
		this.datasourceName = datasourceName;
		this.key = key;
	}

	public Properties getNvPairs() {
		return nvPairs;
	}

	public void setNvPairs(Properties nvPairs) {
		this.nvPairs = nvPairs;
	}

	public OperationFuture<Boolean> save() throws IOException, ConfigurationException{
		return cDriver.getClient(datasourceName).set(key, gson.toJson(nvPairs));
	}
	
	public void load() throws IOException, ConfigurationException{
		String fromCB = (String)cDriver.getClient(datasourceName).get(key);
		nvPairs = gson.fromJson(fromCB, Properties.class);
	}
}
