package org.ebayopensource.couchbase.admin;

import static org.junit.Assert.*;

import org.junit.Test;

public class AdminConsoleTest {

	private static final String LOCAL = "local";
	private static final String LOCAL_CONFIG = "hosts=http://localhost:8091/pools\nbucket=default\npwd=";

	@Test
	public void test() {
		AdminConsole console = new AdminConsole();
		console.addConfig(LOCAL, LOCAL_CONFIG);
		console.doSet(LOCAL, "np", "Test value for key np");
		console.doGet(LOCAL, "np");
		console.doDelete(LOCAL, "np");
		fail("Not yet implemented");
	}

}
